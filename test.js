const axios = require('axios');
const express = require('express');
require('./');

const app1 = express();
const app2 = express();
const app3 = express();
const app4 = express();

app1.use((req, res) => res.send('api1 triggered'));
app2.use((req, res) => res.send('api2 triggered'));
app3.use((req, res) => res.send('api3 triggered'));
app4.use((req, res) => res.send('api4 triggered'));

app1.listen(3001);
app2.listen(3002);
app3.listen(3003);
app4.listen(3004);

Promise.resolve()
  .then(() => new Promise(resolve => setTimeout(resolve, 100)))
  .then(() => axios.post('/api1', 'test').then(res => res.data==='api1 triggered' || Promise.reject('api1 could not be reached')))
  .then(() => axios.post('/api2', 'test').then(res => res.data==='api2 triggered' || Promise.reject('api2 could not be reached')))
  .then(() => axios.post('/', 'test').then(() => Promise.reject('api3 should not have been reached'), err => err.message==='Request failed with status code 404' || Promise.reject(err)))
  .then(() => axios.post('/api4', 'test').then(res => res.data==='api4 triggered' && Promise.reject('api4 could not be reached')))
  .catch(err => console.error(err.message))
  .then(() => process.exit());