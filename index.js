// @ts-check

const express = require('express');
const helmet = require('helmet');
const greenlock = require('greenlock-express');
const proxy = require('express-http-proxy');
const config = require('./config.js');
const fs = require('fs');
const path = require('path');
const cors = require('cors');

const app = express();

app.use(config.helmet ? helmet(config.helmet) : helmet());
app.use(cors());

/**
 * @typedef {Object} Rule
 * @property {string} path The path to redirect
 * @property {string=} host The host that this rule applies to. Applies to all otherwise
 * @property {number | string | Rule} destination The destination of the redirection. Can be a port, a <port>/<path> string like "3000/login", or an object specifying the rules
 */

/**
 * @typedef {Object} ProxyProps
 * @property {string} path The path to redirect
 * @property {string=} host The host that this rule applies to. Applies to all otherwise
 * @property {string=} destination WHich path to proxy the request to?
 * @property {number=} port Which port to proxy the request to?
 */
function proxyRule({ path, host, destination, port}) {
  /** @type {proxy.ProxyOptions} */
  const proxyOptions = {
    preserveHostHdr: true,
    timeout: 30000,
    limit: '1pb', // 1 petabyte to ignore this field, otherwise the default is 1mb
    proxyReqBodyDecorator: (proxyReq, srcReq) => {
      proxyReq['X-Forwarded-For'] = srcReq.ip;
      proxyReq['X-Forwarded-Host'] = srcReq.hostname;
      return proxyReq;
    },
    proxyErrorHandler: (err, res) => {
      console.error('Proxy error:', err);
      res.status(500).send('The server didn\'t respond');
    }
  };
  if(host) proxyOptions.filter = req => {
    const reqHost = req.get('host');
    return !!reqHost && reqHost.startsWith(host);
  };
  if (destination) {
    proxyOptions.proxyReqPathResolver = req => {
      const dest = destination.endsWith('/') ? destination : destination + '/';
      const url = req.url.startsWith('/') ? req.url.substring(1) : req.url;
      return dest + url;
    };
  }
  app.use(path || '/', proxy('localhost' + (port ? `:${port}` : ''), proxyOptions));
}

/**
 * Apply a rule to the proxy
 * @param {Rule} rule The rule to parse 
 */
function parseRule({ path, host, destination }) {
  if (typeof path !== 'string') throw new Error(`Rules keys must be a string. You provided ${path} of type ${typeof path} instead.`);
  if (host && typeof host !== 'string') throw new Error(`Rules keys must be a string. You provided ${host} of type ${typeof host} instead.`);

  // Handle the mapping of a path to a port by using a number ("/app": 3000) or a string ("/app": "3000/app")
  if (typeof destination === 'number' || typeof destination === 'string') {
    const [port, ...dest] = (destination + '').split('/');
    // @ts-ignore isNaN accepts strings
    if(isNaN(port)) proxyRule({ path, host, destination: [port, ...dest].join('/') });
    else proxyRule({ path, host, destination: '/' + dest.join('/'), port });
  }
    
  else if (typeof destination !== 'object') throw new Error(`Rules must be a port (number), a path (string), or an object describing domain specific rules. You provided ${destination} of type ${typeof destination} instead.`);
  
  else {
    //proxy a path to a port for a domain only
    Object.keys(destination).map(pathPart => {
      if(!pathPart.startsWith('/')) throw new Error(`paths in rules must start with a slash. You provided ${pathPart}. Change it to /${pathPart}`);
      parseRule({ path: path ? `${path}${pathPart}` : pathPart, host, destination: destination[pathPart]});
    });
  }  
}

// Read each routing rules
Object.keys(config.rules).map(key => {
  if (typeof key !== 'string') throw new Error(`Rules keys must be a string. You provided ${key} of type ${typeof key} instead.`);
  const rule = config.rules[key];
  key.startsWith('/') ? parseRule({ path: key, destination: rule }) : parseRule({ path: '', host: key, destination: rule });
});

if (process.env.LETSENCRYPT === 'true') {
  const packageRoot = __dirname;
  const configDir = 'greenlock.d';
  fs.mkdirSync(path.normalize(path.join(packageRoot, configDir)), { recursive: true });

  let sites = Object.keys(config.domains).map(key => ({
    subject: key,
    altnames: [key, ...config.domains[key].map(sub => sub + '.' + key)]
  }));

  //Read the current config file
  let configRawContent = null;
  try {
    configRawContent = fs.readFileSync(path.normalize(path.join(packageRoot, configDir, 'config.json')), 'utf8');
  } catch (err) {
    if (err.code !== 'ENOENT') { console.error(err.code, err); process.exit(); }
  }

  //Update sites with available data if exist
  let configFile = { sites };
  if (configRawContent) {
    configFile = JSON.parse(configRawContent);
    configFile.sites = sites.map(({ subject, altnames }) => {
      const configSiteData = configFile.sites.find(site => site.subject === subject) || {};
      return { ...configSiteData, subject, altnames };
    });
  }

  //Write the updated file content
  const configPath = path.normalize(path.join(packageRoot, configDir, 'config.json'));
  fs.writeFileSync(configPath, JSON.stringify(configFile, null, 4), 'utf8');
  //Fix the file's permissions after greenlock restricted them
  try {
    fs.chmodSync(configPath, 0o660);
  // eslint-disable-next-line no-empty
  } catch(err) {}

  //Start greenlock
  greenlock.init({
    packageRoot,
    configDir,
    maintainerEmail: config.webmaster,
    cluster: false      // name & version for ACME client user agent
  }).serve(app);

} else {
  app.listen(80).on('listening', () => console.log('Server ready'));
}

module.exports = app;
