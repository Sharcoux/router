/** Sample config */
module.exports = {
  domains: {                    // List the domains that need let's encrypt certificates
    'mydomain.com': ['www']     // Every domain receives the list of the additional subdomains it should get a certificate for
  },
  webmaster: 'my@email.com',    // The subscriber email
  rules: {                      // List proxy rules
    '/api1': 3001,               // Map a path to a port
    '/api2': 3002,
    'mydomain.com': {           // Set of rules specific to one domain
      '/': 3003
    },
    'localhost': {
      '/api4': '3004/gar'
    }
  },
  // helmet: {                     // (Optional) List the directives to be applied on helmet https://helmetjs.github.io/
  //   contentSecurityPolicy: {
  //     directives: {}
  //   }
  // }
};