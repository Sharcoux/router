# Express Router App

Application that will help serving multiple apps and domains on a single server and handle the let's encrypt certificates for you.

## Getting started

1. Clone the repository: `git clone git@gitlab.com:Sharcoux/router.git` *(You can clone or checkout a tag to target a specific version and prevent breaking changes)*
2. Install dependencies: `cd router && npm i`
3. Enable running node on port 80: `sudo setcap 'cap_net_bind_service=+ep' $(which node)`
4. Ignore changes to config.js: `git update-index --skip-worktree config.js`
5. Edit the config file according to the sample:

```javascript
{
  domains: {                    // List the domains that need let's encrypt certificates
    "mydomain.com": ["www"]     // Every domain receives the list of the additional subdomains it should get a certificate for
  },
  webmaster: "my@email.com",    // The subscriber email
  rules: {                      // List proxy rules
    '/api1': 3001,              // Map a path to a port
    '/api2': 3002,
    'mydomain.com': {           // Set of rules specific to one domain
      '/': 3000
    },
    'mydomain': {               // You can just provide the start of the host name and the rules will apply to all matching domains: mydomain.com, mydomain.net, mydomain.maindomain.com...
      '/api3': 3003,
      '/api4': '3003/api4'      // You can also map an url to another path, on the same port or another one
    },
  }
}
```

6. Set the environment variable `LETSENCRYPT=true` To enable let's encrypt

## What now?

When starting the router server, the let's encrypt certificates will be retrieved automatically.

You can now serve all kind of services on different ports with pm2 for instance. Take a look at this `ecosystem.config.js`:

```javascript
module.exports = {
  apps : [
      {
        name: "router",
        script: "./index.js",
        env: {
            "NODE_ENV": "production",
        },
        cwd: "./router"
      },
      {
        name: "service1",
        script: "./server.js",
        env: {
          "PORT": 3001
        }
      	cwd: "./service1"
      },
      {
        name: "service2",
        script: "./server.js",
        env: {
          "PORT": 3002
        }
      	cwd: "./service2"
      },
      {
        name: "mydomain",
        script: "./server.js",
        env: {
          "PORT": 3003
        }
      	cwd: "./mydomain"
      },
  ]
}
```

**Notice:** As your apps will now be behind a proxy, it might be useful to use the following code to retrieve the original ip from the request

```javascript
app.set('trust proxy', function (ip) {
  if (ip === '127.0.0.1' || ip === '::ffff:127.0.0.1' || ip === '::1') return true // local IPs are trusted
  else return false
})
// now, request.ip in express middleware will resolve to the original client ip.
```
